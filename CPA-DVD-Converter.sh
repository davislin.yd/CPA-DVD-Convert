#!/bin/bash

ParentDirectory=$1

# Check the argument (video directory) is exist.
if [ -z $1 ]; then
    echo 'Please input argument!';
    sleep 3
    exit
fi

# If the video directory is not exist, echo warning message.
if [ ! -d $1 ]; then
    echo "$1 is not a directory, please change another one!";
    sleep 3
    exit
fi

FileList=$(ls $ParentDirectory | grep -i ".iso")
FileCount=$(echo $FileList | wc -l)

echo -e ">> There are $FileCount files on $ParentDirectory\n"
echo -e ">> File list:\n$FileList\n"
sleep 2

for i in ${FileList}; do
    FileFullPath="${ParentDirectory}/${i}"
    FileBaseName=$(echo ${i} | awk -F'.' '{ print $1} ')
    FileFullName=${i}
    
    if [ ! -d ${ParentDirectory}/${FileBaseName} ]; then
        echo -e ">> Creating directory...\n"
        mkdir -p "${ParentDirectory}/${FileBaseName}"
    fi
    
    echo -e ">> Change directory permission to dlin.\n"
    chown -R dlin:dlin "${ParentDirectory}/${FileBaseName}"

    echo -e ">> The ${ParentDirectory}/${FileBaseName} directory is exist!\n"
 
    mount | grep /mnt/isoa > /dev/null
    if [ $? -eq 0 ]; then
       echo -e ">> Unmount /mnt/iso\n"
       umount /mnt/iso
    fi

    echo -e ">> Mount CPA iso to /mnt/iso\n"
    mount "${ParentDirectory}/${i}" /mnt/iso
    
    VOBFileCount=$(ls /mnt/iso/VIDEO_TS/ | grep -i 'vts' | grep -i '.vob' | wc -l)
 
    for j in $(seq 1 $VOBFileCount); do
        if [ $j -lt 10 ]; then
            SourceVOB="/mnt/iso/VIDEO_TS/VTS_0${j}_1.VOB"
            VOBFile="${ParentDirectory}/${FileBaseName}/VTS_0${j}_1.VOB"
            VideoFile="${ParentDirectory}/${FileBaseName}/VTS_0${j}_1.m4v"
            FailLog="${ParentDirectory}/${FileBaseName}/VTS_0${j}_1.log"
        else
            SourceVOB="/mnt/iso/VIDEO_TS/VTS_${j}_1.VOB"
            VOBFile="${ParentDirectory}/${FileBaseName}/VTS_${j}_1.VOB"
            VideoFile="${ParentDirectory}/${FileBaseName}/VTS_${j}_1.m4v"
            FailLog="${ParentDirectory}/${FileBaseName}/VTS_${j}_1.log"
        fi

        if [ ! -f ${VOBFile} ] && [ ! -f ${VideoFile} ]; then
            echo -e ">> Sync all .VOB file which in the DVD iso to ${ParentDirectory}/${FileBaseName}/\n"
            rsync -avzh --progress ${SourceVOB} "${ParentDirectory}/${FileBaseName}/";
            if [ $? -eq 0 ]; then
                echo -e ">> Now starting convert!\n"
                HandBrakeCLI --input ${VOBFile} --output ${VideoFile} --preset "Very Fast 720p30"
                if [ $? -eq 0 ]; then
                    rm -rf ${VOBFile};
                else
                    echo 'This video converting is fail' > ${FailLog}
                fi
            fi
        elif [ -f ${VOBFile} ] && [ -f ${VideoFile} ]; then
            echo -e ">> The m4v file is exist, then delete original .VOB file.\n"
            rm -rf ${VOBFile}
        elif [ -f ${VOBFile} ] && [ ! -f ${VideoFile} ]; then
            echo -e ">> Now starting convert!\n"
            HandBrakeCLI --input ${VOBFile} --output ${VideoFile} --preset "Very Fast 720p30"
            if [ $? -eq 0 ]; then
                rm -rf ${VOBFile};
            else
                echo 'This video converting is fail' > ${FailLog}
            fi
        fi
    done

    echo -e ">> Change permission of ${ParentDirectory}/${FileBaseName} to dlin.\n"
    chown -R dlin:dlin ${ParentDirectory}/${FileBaseName}

    echo -e ">> Unmount /mnt/iso/\n"
    umount /mnt/iso
    sleep 2;
done
